(function(){
	
	var gallery = {
		"dom" : {
			"textItem" : document.getElementsByClassName("gallery-content-item"),
			"pictureItem" : document.getElementsByClassName("gallery-propic"),
			"nextbtn" : document.getElementsByClassName("gallery-next"),
			"prevbtn" : document.getElementsByClassName("gallery-prev"),
			"viewcta" : document.getElementsByClassName("view-cta"),
			"calendarwrapper" : document.getElementsByClassName("calendar-wrapper"),
			"calendarClose" : document.getElementsByClassName("calendar-close-hotspot"),
			"calendarCloseBtn" : document.getElementsByClassName("calendar-close-btn")
		},
		"boot" : function(){
			
			var dom = gallery.dom,
				activeItem = 0,
				noOfItem =  dom.textItem.length,
				calendarframe;
			
			for(var i = 0; i < noOfItem; i++)
			{
				if(i == 0)
				{
					dom.textItem[i].classList.add("active");
					dom.pictureItem[i].classList.add("active");
				}
					
				else
				{
					dom.textItem[i].classList.add("inactive");
					dom.pictureItem[i].classList.add("inactive");
				}
					
			}
			
			dom.nextbtn[0].addEventListener("click",handleArrowClick);
			dom.prevbtn[0].addEventListener("click",handleArrowClick);
			
			function handleArrowClick(e){
				
				var arrow = this.getAttribute("data-arrow");
				
				dom.textItem[activeItem].classList.remove("active");
				dom.pictureItem[activeItem].classList.remove("active");
				dom.textItem[activeItem].classList.add("inactive");
				dom.pictureItem[activeItem].classList.add("inactive");
				
				if(arrow == "next")
					activeItem = (++activeItem >= noOfItem)? 0 : activeItem;
				else if(arrow == "prev")
				activeItem = (--activeItem < 0)? (noOfItem-1) : activeItem;
				
				dom.textItem[activeItem].classList.add("active");
				dom.pictureItem[activeItem].classList.add("active");
				dom.textItem[activeItem].classList.remove("inactive");
				dom.pictureItem[activeItem].classList.remove("inactive");
				
			}
			
			for(var i = 0; i < dom.viewcta.length; i++)
			{
				dom.viewcta[i].addEventListener("click",handleViewCalendarClick);
				dom.calendarCloseBtn[i].addEventListener("click",closeCallendarPopup);
			}

			dom.calendarClose[0].addEventListener("click",closeCallendarPopup);
			
			
			function closeCallendarPopup()
			{
				dom.calendarwrapper[0].style.opacity = calendarframe.style.opacity = "0";
				dom.calendarwrapper[0].style.zIndex = calendarframe.style.zIndex = "-1";
			}
			
			function handleViewCalendarClick(e)
			{
				var btnName = this.getAttribute("data-btn-name"),
					calendarFrames = document.getElementsByClassName("calendar-content");
					
				for(var i = 0; i < calendarFrames.length; i++)
				{
					calendarFrames[i].style.display = "none";
				}
				
				calendarframe = document.getElementsByClassName(btnName)[0];
				calendarframe.style.display = "block";
				dom.calendarwrapper[0].style.opacity = calendarframe.style.opacity = "1";
				dom.calendarwrapper[0].style.zIndex = calendarframe.style.zIndex = "5";
			}
		}
	};

	window.addEventListener("DOMContentLoaded",function(){
		gallery.boot();
	});
	
})();