function Toggle(state) {
    this.shown = this.state = state || false;
    this.msg = '';
}
Toggle.prototype.show = Toggle.prototype.activate = function () {
    this.shown = this.state = true;
}
Toggle.prototype.hide = Toggle.prototype.deactivate = function () {
    this.shown = this.state = false;
    this.msg = '';
}
Toggle.prototype.showPop = function () {
    this.shown = this.state = true;
    //document.body.style.overflowY = "hidden";
}
Toggle.prototype.showMsg = function (msg) {
    this.shown = this.state = true;
    this.msg = msg;
    //document.body.style.overflowY = "hidden";
}
Toggle.prototype.hidePop = function () {
    this.shown = this.state = false;
    this.msg = '';
    //document.body.style.overflowY = "auto";
}

function Pagination(noOfItemsInaPage) {
    this.active = [];
    this.items = [];
    this.pages = [];

    // for(;originalData.length != 0;){

    // 	if(newArray.length < 4){
    // 		newArray.push(originalData.shift());
    // 	} else {
    // 		_selfData.items.push(newArray);
    // 		newArray = [];
    // 	}
    // }

    // if(newArray.length > 0)
    // 	_selfData.items.push(newArray);

    this.set = function (newItems) {
        this.items = newItems || this.items;

        var newArray = [];

        for (; newItems.length != 0;) {
            if (newArray.length < noOfItemsInaPage) {
                newArray.push(newItems.shift());
            } else {
                this.pages.push(newArray);
                newArray = [];
            }
        }

        if (newArray.length > 0)
            this.pages.push(newArray);

        this.activeIndex = 0;

        this.active = this.pages[this.activeIndex];

    }

    this.next = function () {
        this.activeIndex = (this.activeIndex < this.pages.length - 1) ? ++this.activeIndex : this.activeIndex;

        this.active = this.pages[this.activeIndex];
    }

    this.prev = function () {
        this.activeIndex = (this.activeIndex > 0) ? --this.activeIndex : this.activeIndex;
        this.active = this.pages[this.activeIndex];
    }
}

var app = angular.module("app", []);

//Topmost Controller
app.controller("amdocs", ["$scope", "$http", function (s, $http) {

    s.loading = new Toggle(false);

    s.redirect = function (e) {
        window.location.href = e;
    }

    s.api = config.api || {};

}]);


app.controller("categoryMenu", ["$scope", "$http", function (s, $http) {

    var parent = s.$parent;

    function Filter(enabled) {

        var _thisFilter = this;

        this.items = [];
        this.value = null;
        this.enabled = enabled || false;

        this.enable = function () {
            this.enabled = true;
        }

        this.setItems = function (items) {
            this.items = items;
            this.enabled = true;
        }
        this.unsetItems = function () {
            this.items = [];
            this.enabled = false;
        }

        this.change = function () {
            //console.log(_thisFilter);
        }
    }

    //Toggle which will show and hide the filter section as a whole.
    s.filter = new Toggle(false);

    s.setFilter = function (x, originalData) {

        if (!x.showFilter) {
            s.filter.hide();
            return;
        }

        s.filter.show();

        //console.log(originalData);

        var flag = '';
        var catItems = [];
        var points = {
            max: 0,
            min: NaN
        }

        originalData.forEach(function (key) {


            if (key.Category != '' && key.Category != null && key.Category != undefined) {

                if (catItems.length > 0) {

                    flag = true;
                    catItems.forEach(function (key2) {
                        //console.log(key2);
                        if (key2.label == key.Category)
                            flag = false;
                    });

                    if (flag == true)
                        catItems.push({ label: key.Category });

                } else {
                    console.log("else");
                    catItems.push({ label: key.Category });
                }
            }

            if (key.Points != '' && key.Points != NaN && key.Points != undefined) {
                points.max = (points.max < key.Points) ? key.Points : points.max;
                //console.log(isNaN(points.min));
                points.min = (isNaN(points.min)) ? key.Points : ((points.min > key.Points) ? key.Points : points.min);
            }

        });

        //console.log(points);
        if (catItems.length > 0)
            catItems.unshift({ label: "all" });

        s.categoryFilter.setItems(catItems);
        //							//Minimum	//Maximum	//Default value of slider
        s.pointSliderFilter.setItems([points.min, points.max, points.max]);
        s.sortFilter.enable();
        s.searchBoxFilter.enable();

    }

    s.categoryFilter = new Filter(false);
    s.sortFilter = new Filter(false);
    s.searchBoxFilter = new Filter(false);
    s.pointSliderFilter = new Filter(false);


    //THIS FUNCITON INVOKE ON EVERY FILTER CHANGE EVENTS.
    s.filterChange = function () {


        var filteredData = [],
			pass = false;

        s.categoryItems.originalData.forEach(function (key) {
            //	console.log(key.category);
            //console.log(s.pointSliderFilter.value);
            if (s.categoryFilter.value != null && s.categoryFilter.value.label != 'all' && s.categoryFilter.value.label != key.Category)
                return;
            else if (s.searchBoxFilter.value != null && key.Name.toLowerCase().indexOf(s.searchBoxFilter.value.toLowerCase()) == -1)
                return;
            else if (s.pointSliderFilter.value != null && key.Points > s.pointSliderFilter.value)
                return;

            filteredData.push(key);
        });

        if (s.sortFilter.value != null) {
            //console.log("Sort filter not null");

            var tempObj;

            if (s.sortFilter.value.value == 1) {
                //Ascending Sort
                for (var i = 0; i < filteredData.length; i++) {
                    for (var j = i; j < filteredData.length; j++) {
                        if (filteredData[i].Points > filteredData[j].Points) {
                            tempObj = filteredData[i];
                            filteredData[i] = filteredData[j];
                            filteredData[j] = tempObj;
                        }
                    }
                }
            } else if (s.sortFilter.value.value == 2) {
                for (var i = 0; i < filteredData.length; i++) {
                    for (var j = i; j < filteredData.length; j++) {
                        if (filteredData[i].Points < filteredData[j].Points) {
                            tempObj = filteredData[i];
                            filteredData[i] = filteredData[j];
                            filteredData[j] = tempObj;
                        }
                    }
                }
            }

        }

        //console.log(s.searchBoxFilter.value);
        console.log(filteredData);

        s.makePagination(filteredData);

    }



    function Data(apiUrl) {

        var _selfData = this;
        _selfData.items = [];
        this.apiUrl = apiUrl;
        _selfData.active = null;

        _selfData.ajaxSuccessFun = function () { };

        _selfData.getItems = function (params, postData) {

            _selfData.items = [];	//Empty the list first
            if (params && params.categoryId == -1)
                apiUrl = config.api.GetVoucherItemsMenu;
            else if (params)
                apiUrl = config.api.GetClassItemsMenu;
            parent.loading.showPop();

            $http({
                url: apiUrl,
                params: params,
                data: postData || {},
                method: (postData == undefined || postData == null) ? "GET" : "POST",
                headers: {
                    'Content-Type': "application/json"
                }
            }).then(
				_selfData.ajaxSuccessFun,
                    function (failed) {
                        parent.loading.hidePop();
                    }
                );
        }
    }



    s.sortFilter.items = [
    {
        label: "Low to High",
        value: 1
    },
    {
        label: "High to Low",
        value: 2
    },

    ];

    s.categoryMenu = new Data(config.api.GetCategoryItemsMenu);

    s.categoryItems = new Data(config.api.GetClassItemsMenu);


    s.categoryMenu.baseImgUrl = config.api.categoryMenuBaseImgUrl;
    s.categoryItems.baseImgUrl = config.api.categoryItemsbaseImgUrl;

    s.bookingPage = config.api.bookingPage;
    s.indexPage = config.api.indexPage;

    //Overwrite success function for Category Menu model.
    s.categoryMenu.ajaxSuccessFun = function (success) {
        var _selfData = s.categoryMenu;
        console.log(success.data);
        _selfData.items = success.data;
        parent.loading.hidePop();
    }

    //Overwrite success function for Category Items model.
    s.categoryItems.ajaxSuccessFun = function (success) {

        var _selfData = s.categoryItems;

        _selfData.originalData = angular.copy(success.data);

        s.makePagination(success.data);

        s.setFilter(s.categoryMenu.active, _selfData.originalData);

        parent.loading.hidePop();


    }

    s.makePagination = function (originalData) {
        //Groupping the array data into 4 items each to feed a table row with 4 column.

        console.log();
        var _selfData = s.categoryItems,
            newArray = [];

        //_selfData.items = [];
        _selfData.items = originalData;

        // for(;originalData.length != 0;){

        // 	if(newArray.length < 4){
        // 		newArray.push(originalData.shift());
        // 	} else {
        // 		_selfData.items.push(newArray);
        // 		newArray = [];
        // 	}
        // }

        // if(newArray.length > 0)
        // 	_selfData.items.push(newArray);

        _selfData.page = new Pagination(8);
        _selfData.page.set(originalData);

        console.log(_selfData.page);

    }




    //INITIATE
    s.categoryMenu.getItems();

}]);


//CONTROLLER FOR TRENDING ITEMS
app.controller("trendingItems", ["$scope", "$http", function (s, $http) {


    function Data(apiUrl) {

        var _selfData = this;

        _selfData.items = [];

        _selfData.getItems = function () {


            $http({
                url: apiUrl,
                method: "GET"
            }).then(
				function (success) {
				    _selfData.items = success.data;
				},
				function (failed) { }
			);

        }

    }

    s.trendingItems = new Data(config.api.getclasstrendingitemsmenu);
    s.trendingItems.getItems();
    s.trendingItems.baseImgUrl = config.api.categoryItemsbaseImgUrl;
    s.bookingPage = config.api.bookingPage;

    console.log(1);

}]);


//CONTROLLER FOR BOOKING PAGE
app.controller("bookingclassController", ["$scope", "$http", "$log", function (s, $http, $log) {

    var log = $log.log,
		$scope = s;
    s.errorMsg = new Toggle(false);
    s.successMsg = new Toggle(false);

    s.bookingItem = dynamicData.bookingItem;
    var bookApi = config.api.bookClassItem;

    //console.log(1);



    // s.deliveryInfo = new Form(dynamicData.deliverAddress);

    //console.log(s.deliveryInfo);
    s.initiateBooking = function () {

        s.successMsg.hide();
        s.errorMsg.hide();
        s.referFriend.errorMsg.hide();
        s.referdBy.errorMsg.hide();
        s.showBookSum.show();
        s.referFriend.email = '';
        s.referdBy.email = '';
        s.showBookSum.show()
    }

    $scope.formDisabled = true;

    s.showBookSum = new Toggle(false);
    s.bookedMsg = new Toggle(false);
    s.referPrompt = new Toggle(false);

    s.referFriend = new Toggle(false);
    s.referFriend.email = '';
    s.referFriend.errorMsg = new Toggle(false);


    s.referdBy = new Toggle(false);
    s.referdBy.email = '';
    s.referdBy.errorMsg = new Toggle(false);

    s.prompt = new Toggle(false);
    s.messagePop = new Toggle(false);

    s.userInfo = dynamicData.userInfo;

    $scope.pops = {
        showBookSum: false,
        showMesg: false,
        showReferPrmt: false,
        showReferForm: false,
        showReferByForm: false,
    }

    s.showBookSum.purchaseReconfirm = function () {
        if (s.userInfo.Points >= 0)
            s.prompt.showMsg("The purchased points will be deducted from your next salary. <br>Are you sure you want to purchase " + (s.bookingItem.userSelected.Points - s.userInfo.Points) + " points?<br> 1 Point is equivalent to 1 Rupee for Salary Deductions");
        else
            s.prompt.showMsg("The purchased points will be deducted from your next salary. <br>Are you sure you want to purchase " + (s.bookingItem.userSelected.Points) + " points?<br> 1 Point is equivalent to 1 Rupee for Salary Deductions");
        s.showBookSum.hide();
        s.prompt.yes = function () {
            $scope.confirmBook();
            s.prompt.hide();
        }

        s.prompt.no = function () {
            s.prompt.hide();
        }

    }


    $scope.confirmBook = function () {
        var _selfBook = this;
        _selfBook.errorMsg = new Toggle(false);

        console.log(s.bookingItem);
        console.log("confirm book");

        s.$parent.loading.show();

        var overdraftPointsValue;
        if (s.userInfo.Points < 0) {
            overdraftPointsValue = s.bookingItem.userSelected.Points
        }
        else {
            overdraftPointsValue = s.bookingItem.userSelected.Points - s.userInfo.Points;
        }

        console.log(bookApi);

        $http({
            url: bookApi,
            method: "POST",
            params: {
                activityId: s.bookingItem.userSelected.itemId,
                overdraftPoints: overdraftPointsValue
            },
            data: {
                activityId: s.bookingItem.userSelected.itemId,
                overdraftPoints: overdraftPointsValue
            }
        }).then(
            function (success) {
                if (success.data.Result == "Success") {
                    s.successMsg.showMsg(success.data.Message);
                    s.bookedMsg.show();
                    s.showBookSum.hide();

                    s.bookingItem.userSelected.IsBooked = true;
                    s.bookingItem.userSelected = null;
                    getUserBalancePOints();
                }
                else {
                    s.bookedMsg.hide();
                    s.errorMsg.showMsg(success.data.Message);
                    s.showBookSum.show();
                    s.$parent.loading.hide();
                }
            },
                function (failed) {
                    s.$parent.loading.hide();
                }
                );
    }

    function getUserBalancePOints() {
        $http({
            url: config.api.GetCurrentBalancePoints,
            method: "GET"
        }).then(
            function (success) {
                console.log(success.data);
                s.userInfo.Points = parseInt(success.data);
                s.$parent.loading.hide();
            }
        );


    }


    $scope.referBySubmit = function (e) {

        //$scope.pops.showReferPrmt = true;
        //$scope.pops.showReferByForm = false;

        e.preventDefault();
        log("Submit");
        console.log(s.referdBy.email);

        $http({
            url: config.api.checkreferedBy,
            method: "GET",
            params: {
                activityId: s.bookingItem.userSelected.itemId,
                referredByFriendEmailId: s.referdBy.email,
            }
        }).then(
    function (success) {
        console.log(success.data);

        if (success.data.Result.toLowerCase() == 'error') {
            s.referdBy.errorMsg.showMsg(success.data.Message);
        } else {
            s.referdBy.hide();
            s.referPrompt.show();
        }
    },
			function (failed) {
			}
		);
    }

    $scope.referFriendSubmit = function (e) {
        e.preventDefault();
        log("Submit");

        $http({
            url: config.api.referFriend,
            method: "GET",
            params: {
                activityId: s.bookingItem.userSelected.itemId,
                itemId: s.bookingItem.userSelected.itemId,
                friendEmailId: s.referFriend.email,
            }
        }).then(
                        function (success) {
                            console.log(success.data);

                            if (success.data.Result.toLowerCase() == 'error') {
                                s.referFriend.errorMsg.showMsg(success.data.Message);
                            } else {
                                s.referFriend.hide();
                            }
                        },
    function (failed) {
    }
    );

        //$scope.pops.showReferForm = false;
    }

    $scope.stopPropagate = function (e) {
        e.stopPropagation();
    }

    $scope.scrollToView = function (className) {
        var element = document.getElementsByClassName(className)[0],
            scrollHeight = element.scrollHeight,
            offsetTop = element.offsetTop - 100,
        speed = 750;

        Velocity(element, {
            "height": scrollHeight + "px"
        }, speed, function () {
            element.style.height = "auto";
        });

        Velocity(document.body, "scroll", {
            duration: speed, offset: offsetTop
        });
    }

    s.collapse = function (className) {
        var element = document.getElementsByClassName(className)[0],
        speed = 750;

        if (element.offsetHeight == 0)
            return;

        Velocity(element, { "height": "0px" }, speed);

        //Velocity(document.body,"scroll", { duration: speed, offset: offsetTop });
    }

    console.log("Booking");

}]);


//CONTROLLER FOR BOOKING PAGE
app.controller("bookingvoucherController", ["$scope", "$http", "$log", function (s, $http, $log) {

    var log = $log.log,
		$scope = s;
    s.lastNamePlaceHolder = 'Last Name';
    s.bookingItem = dynamicData.bookingItem;
    var bookApi = config.api.bookVoucherItem;
    s.errorMsg = new Toggle(false);
    s.successMsg = new Toggle(false);

    //console.log(1);

    function Form(data) {
        var _selfForm = this;

        _selfForm.mode = '';
        _selfForm.data = data;
        _selfForm.message = new Toggle(false);
        _selfForm.originData = null;

        _selfForm.emptyFields = function () {

            //console.log(_selfForm.data);
            _selfForm.originData = angular.copy(_selfForm.data);

            s.lastNamePlaceHolder = 'Last Name*'
            $("#State").removeAttr("disabled");
            _selfForm.data.email = '';
            _selfForm.data.mobile = '';
            _selfForm.data.state = '';
            _selfForm.data.city = '';
            _selfForm.data.address1 = '';
            _selfForm.data.address2 = '';
            _selfForm.data.firstName = '';
            _selfForm.data.lastName = '';
            _selfForm.data.pincode = '';
            _selfForm.data.country = '';
        }
        _selfForm.cancelAddNew = function () {
            s.lastNamePlaceHolder = 'Last Name';
            if (_selfForm.originData != null) {
                _selfForm.data = _selfForm.originData;
                _selfForm.originData = null;
                
            }

        }

        _selfForm.validate = function () {
            
            s.successMsg.showMsg('');
            s.errorMsg.showMsg('');
            var data = _selfForm.data;
            var mode = s.deliveryInfo.mode;
           
            if (mode != '' && data.lastName == '') {
                    _selfForm.message.showMsg("Please complete the Delivery Address form");
                    return;
                }
            else if ((data.voucherType == 'EGV') && (data.email == '' || data.mobile == '' || data.firstName == '')) {
                _selfForm.message.showMsg("Please complete the Delivery Address form");
                return;
            }
            else if ((data.voucherType != 'EGV' )&&( data.email == '' || data.mobile == '' || data.state == '' || data.city == '' || data.address1 == '' || data.firstName == '' || data.pincode == '')) {

                _selfForm.message.showMsg("Please complete the Delivery Address form");
                return;
            }
            else if (data.mobile != '' && (data.mobile.toString().split("").length > 10 || data.mobile.toString().split("").length < 10)) {
                _selfForm.message.showMsg("Phone number must have 10 digits.");
                return;
            }
            else if ((data.voucherType != 'EGV' )&& (data.pincode != '' && (data.pincode.toString().split("").length > 6 || data.pincode.toString().split("").length < 6))) {
                _selfForm.message.showMsg("Pin Code must have 6 digits.");
                return;
            }
            else {
                _selfForm.message.hide();
               
            }


            console.log(s.bookingItem);

            s.showBookSum.show();

        }
    }

    s.deliveryInfo = new Form(dynamicData.deliverAddress);

    //console.log(s.deliveryInfo);

    $scope.formDisabled = true;

    s.showBookSum = new Toggle(false);
    s.bookedMsg = new Toggle(false);
    s.referPrompt = new Toggle(false);

    s.referFriend = new Toggle(false);
    s.referFriend.email = '';
    s.referFriend.errorMsg = new Toggle(false);


    s.referdBy = new Toggle(false);
    s.referdBy.email = '';
    s.referdBy.errorMsg = new Toggle(false);

    s.prompt = new Toggle(false);
    s.messagePop = new Toggle(false);

    s.userInfo = dynamicData.userInfo;

    $scope.pops = {
        showBookSum: false,
        showMesg: false,
        showReferPrmt: false,
        showReferForm: false,
        showReferByForm: false,
    }

    s.bookBtnClick = function(){

        if(s.bookingItem.bookingTerms != null && s.bookingItem.bookingTerms != '' && s.bookingItem.bookingTerms != undefined){

            s.prompt.showMsg(s.bookingItem.bookingTerms);
            s.prompt.yes = function(){
                s.scrollToView('address_section');
                s.prompt.hide();
            }

            s.prompt.no = function () {
                s.prompt.hide();
            }
        }
    }

    s.showBookSum.purchaseReconfirm = function () {
        if (s.userInfo.points < s.bookingItem.userSelected.points && s.userInfo.points >= 0)
            s.prompt.showMsg("<p class='booking-item-description'>The purchased points will be deducted from your next salary. <br>Are you sure you want to purchase " + (s.bookingItem.userSelected.points - s.userInfo.points) + " points?<br> 1 Point is equivalent to 1 Rupee for Salary Deductions</p>");
        else
            s.prompt.showMsg("<p class='booking-item-description'>The purchased points will be deducted from your next salary. <br>Are you sure you want to purchase " + (s.bookingItem.userSelected.points) + " points?<br> 1 Point is equivalent to 1 Rupee for Salary Deductions</p>");

        s.showBookSum.hide();
        s.prompt.yes = function () {
            $scope.confirmBook();
            s.prompt.hide();
        }

        s.prompt.no = function () {
            s.prompt.hide();
        }

    }


    $scope.confirmBook = function () {
        console.log('confirm');
        //$scope.pops.showBookSum = false;
        //$scope.pops.showMesg = true;
        console.log(config);
        var _selfBook = this;
        _selfBook.errorMsg = new Toggle(false);
        var overDraft = 0;

        if (s.userInfo.points < 0) {
            overDraft = s.bookingItem.userSelected.points;
        }
        else {
            overDraft = s.bookingItem.userSelected.points - s.userInfo.points;
        }
        var deliveryInfo = {
            email: s.deliveryInfo.data.email,
            mobile: s.deliveryInfo.data.mobile,
            state: $("#State option:selected").val(),
            city: s.deliveryInfo.data.city,
            address1: s.deliveryInfo.data.address1,
            address2: s.deliveryInfo.data.address2,
            firstName: s.deliveryInfo.data.firstName,
            lastName: s.deliveryInfo.data.lastName,
            pincode: s.deliveryInfo.data.pincode,
            country: 'India',
            itemId: s.bookingItem.userSelected.itemId,
            totalPoints: s.bookingItem.userSelected.points,
            quantity: s.bookingItem.userSelection.value,
            mode: s.deliveryInfo.mode,
            overDraftPoints: overDraft,
            voucherType: s.deliveryInfo.data.voucherType
        }

        console.log(deliveryInfo);
        console.log(s.bookingItem);
        console.log("confirm book");

        s.$parent.loading.show();

        $http({
            url: bookApi,
            method: "POST",
            data: {
                deliveryInfo: deliveryInfo
            }
        }).then(
            function (success) {
                if (success.data.error == false) {
                    s.successMsg.showMsg(success.data.result);
                    s.RegistrationConfirmationNumber = success.data.result;
                    s.bookedMsg.show();
                    s.showBookSum.hide();
                    s.$parent.loading.hide();
                }
                else {
                    s.bookedMsg.hide();
                    s.errorMsg.showMsg(success.data.result);
                    s.showBookSum.show();
                    s.$parent.loading.hide();
                }
            },
                function (failed) {
                    s.$parent.loading.hide();
                }
                );
    }
    $scope.referBySubmit = function (e) {
        e.preventDefault();
        log("Submit");
        console.log(s.referdBy.email);

        $http({
            url: config.api.checkreferedBy,
            method: "GET",
            params: {
                activityId: s.bookingItem.userSelected.itemId,
                itemId: s.bookingItem.userSelected.itemId,
                referredByFriendEmailId: s.referdBy.email,
            }
        }).then(
    function (success) {
        console.log(success.data);

        if (success.data.Result.toLowerCase() == 'error') {
            s.referdBy.errorMsg.showMsg(success.data.Message);
        } else {
            s.referdBy.hide();
            s.referPrompt.show();
        }
    },
			function (failed) { }
		);
    }

    $scope.referFriendSubmit = function (e) {
        e.preventDefault();
        log("Submit");

        $http({
            url: config.api.referFriend,
            method: "GET",
            params: {
                activityId: s.bookingItem.userSelected.itemId,
                itemId: s.bookingItem.userSelected.itemId,
                friendEmailId: s.referFriend.email,
            }
        }).then(
                        function (success) {
                            console.log(success.data);

                            if (success.data.Result.toLowerCase() == 'error') {
                                s.referFriend.errorMsg.showMsg(success.data.Message);
                            } else {
                                s.referFriend.hide();
                            }
                        },
    function (failed) { }
		);

    }

    $scope.stopPropagate = function (e) {
        e.stopPropagation();
    }

    $scope.scrollToView = function (className) {
        var element = document.getElementsByClassName(className)[0],
            scrollHeight = element.scrollHeight,
            offsetTop = element.offsetTop - 100,
            speed = 750;

        Velocity(element, { "height": scrollHeight + "px" }, speed, function () {
            element.style.height = "auto";
        });

        Velocity(document.body, "scroll", { duration: speed, offset: offsetTop });
    }

    s.collapse = function (className) {
        var element = document.getElementsByClassName(className)[0],
			speed = 750;

        if (element.offsetHeight == 0)
            return;

        Velocity(element, { "height": "0px" }, speed);

        //Velocity(document.body,"scroll", { duration: speed, offset: offsetTop });
    }

    console.log("Booking");
    s.checkForUserPoints = function () {
        if (s.userInfo.points < s.bookingItem.userSelected.points && s.userInfo.points >= 0)
            return true;
        else
            return false;


    };
}]);


//THIS CONTROLLER IS FOR BOOKING PAGE.
app.controller("myaccount", ["$scope", "$http", "$log", function (s, $http, $log, $scope) {

    s.baseImgUrl = config.api.categoryItemsbaseImgUrl;

    s.tab = {
        active: 'myprofile',
    }

    function BookedItems(apiUrl) {
        // var apiUrl = config.api.getBookedItems;

        //console.log(apiUrl);

        var _self = this;

        _self.getItems = function () {

            $http({
                url: apiUrl,
                method: "GET"
            }).then(
                function (success) {

                    _self.items = success.data;
                    if (success.data[0] != undefined) {
                        s.userProfileInfo.data.pointEarned = success.data[0].userPoints;
                    }
                    else {
                        s.userProfileInfo.data.pointEarned = success.data;
                    }
                    console.log(s.BookedItems);
                },
    function (failed) {
    }
    );
        }

        _self.getItems();

    }

    function Cancel() {

        this.shown = false;
        this.cancelledMessage = new Toggle(false);

        var apiUrl = config.api.cancelItem;

        this.data = {
        };
        var typeCancel = '';
        this.show = function (x, type) {

            // console.log(x)
            this.shown = true;
            this.data = x;
            if (type != undefined && type == 'voucher') {
                apiUrl = config.api.cancelVoucherItem;
                typeCancel = type;
            }
        }

        this.hide = function () {
            this.shown = false;
            //this.data = {};
        }

        var _self = this;

        this.confirmCancel = function () {
            $http({
                url: apiUrl,
                params: {
                    id: _self.data.Id
                },
                method: "GET"
            }).then(
				function (success) {
				    _self.hide();
				    console.log(s.classBooked);
				    if (typeCancel == '') {
				        s.classBooked.getItems();
				    }
				    else {
				        s.voucherBooked.getItems();
				    }
				    _self.cancelledMessage.showMsg(success.data.Message);
				},
                    function () {
                    }
                );

        }
    }

    function ChangePassword() {
        var _self = this;

        function Input() {
            this.value = '';
        }

        _self.oldPassword = new Input();
        _self.newPassword = new Input();
        _self.repeatPassword = new Input();
        _self.errorMsg = new Toggle();
        _self.successMsg = new Toggle();

        //{"OldPassword":"a","NewPassword":"a","ConfirmPassword":"a"}

        _self.save = function () {

            console.log("save");
            _self.errorMsg = new Toggle(false);
            $http({
                url: config.api.changePassword,
                method: "POST",
                data: {
                    OldPassword: _self.oldPassword.value,
                    NewPassword: _self.newPassword.value,
                    ConfirmPassword: _self.repeatPassword.value,
                },
                headers: {
                    'Content-Type': 'application/json'
                }
            }).then(
                        function (success) {
                            console.log(success.data.success != '' && success.data.success != undefined);
                            if (success.data.success == '' && success.data.error != '' && success.data.error != undefined && success.data.error != null) {
                                _self.errorMsg.showMsg(success.data.error);
                                console.log("Failed save");
                            } else if (success.data.success != '' && success.data.success != undefined) {
                                console.log("Success save");
                                _self.successMsg.showMsg(success.data.success);
                            }
                        },
				function () {

				}
			);
        }
    }

    function UserProfileInfo() {
        var _self = this;

        _self.data = dynamicData.userInfo;
        _self.errorMsg = new Toggle(false);
        _self.successMsg = new Toggle(false);
        _self.data.country = "India";
        _self.mode = '';

        $("#profileEditCta").click(function () {
            $("#State").removeAttr("disabled");
        })


        _self.save = function (data) {
            console.log(data)
            _self.data = data
            _self.data.State = $("#State option:selected").val()

            //e.preventDefault();
            //console.log("save");
            //console.log(_self.data);
            //console.log(config.api.saveUserInfo);
            _self.successMsg.hide();
            _self.errorMsg.hide();

            $http({

                url: config.api.saveUserInfo,
                method: "POST",
                data: {
                    userInfo: _self.data,
                },
                headers: {
                    "Content-Type": "application/json"
                }
            }).then(

                    function (success) {
                        console.log(success);
                        console.log(success.data);
                        if (success.data.error) {
                            _self.errorMsg.showMsg(success.data.result);
                            _self.successMsg.showMsg('');
                        }
                        else {
                            console.log("Saved");
                            _self.errorMsg.showMsg('');
                            _self.successMsg.showMsg(success.data.result);
                            $("#State").attr("disabled", true);
                            _self.mode = '';
                        }


                    },
              function (failed) {
                  console.log("Technical reason");
                  _self.errorMsg.showMsg("Failed to save due to technical reason.");
              }

            );

        }

    }
    //	var apiUrl = config.api.getBookedClassItems;

    //_self.userProfileInfo.mode = function () {
    //    alert("");
    //}
    s.userProfileInfo = new UserProfileInfo();
    s.changePassword = new ChangePassword();
    s.cancel = new Cancel();
    s.classBooked = new BookedItems(config.api.getBookedClassItems);
    s.voucherBooked = new BookedItems(config.api.getBookedVoucherItems);
    console.log(s.classBooked);
    console.log(s.voucherBooked);
}]);

