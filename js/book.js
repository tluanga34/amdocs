(function(){
	
	angular.module("bookingApp",[]).controller("bookingController",function($scope, $http, $log){
		
		var log = $log.log;
		
		$scope.formDisabled = true;
		
		$scope.pops = {
			showBookSum : false,
			showMesg : false,
			showReferPrmt : false,
			showReferForm : false,
			showReferByForm : false,
		}
		
		$scope.confirmBook = function(){
			$scope.pops.showBookSum = false;
			$scope.pops.showMesg = true;
		}
		$scope.referBySubmit = function(e){
			
			$scope.pops.showReferPrmt = true;
			$scope.pops.showReferByForm = false;
			
			e.preventDefault();
			log("Submit");
		}
		
		$scope.referFriendSubmit = function(e){
			e.preventDefault();
			log("Submit");
			
			$scope.pops.showReferForm = false;
		}
					
		$scope.stopPropagate = function(e){
			e.stopPropagation();
		}
		
		$scope.scrollToView = function(className){
			var element = document.getElementsByClassName(className)[0],
				scrollHeight = element.scrollHeight,
				offsetTop = element.offsetTop - 100,
				speed = 750;
			
			Velocity(element, {"height":scrollHeight+"px"},speed,function(){
				element.style.height = "auto";
			});
			
			Velocity(document.body,"scroll", { duration: speed, offset: offsetTop });
		}
		
		console.log("Booking");
		
	});
	
	
})();