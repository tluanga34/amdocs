var config = {
	
	api : {
				
		/*
		GetCategoryItemsMenu : "/TrainingActivity/GetCategoryItemsMenu",
		categoryMenuBaseImgUrl : "/FileStorage/CategoryImages/",
		GetClassItemsMenu : "/TrainingActivity/GetClassItemsMenu",
		categoryItemsbaseImgUrl : "/FileStorage/ClassImages/",
		getclasstrendingitemsmenu : '/trainingactivity/getclasstrendingitemsmenu',
		getMoreActivityItemsMenu : '/TrainingActivity/GetMoreActivityItemsMenu',
		bookItem : '/UserTrainingActivity/BookUserTrainingActivity',
		getBookedItems : "User/GetActivityBookedByUser",
		bookingPage : "/UserTrainingActivity/BookNow",
		cancelItem : "/UserTrainingActivity/CancelUserTrainingActivity",
		indexPage : "/dashboard/index",
		bookVoucher : "bookVoucher.html",
		
		//Params activityId=2069&referredByFriendEmailId=tluanga34gmail.com
		checkreferedBy : "UserTrainingActivity/ReferredByFriend",
		
		referFriend : "UserTrainingActivity/ReferFriend",
		
		changePassword : "/user/changepassword",
		
		saveUserInfo : "/user/saveUserInfo", //This need to be created
		
		//*/
		
		//Uncomment aBOVE when deployed in a server
		//COMMENT ALL BELOW IN A SERVER//
		
		// /*
		
		GetCategoryItemsMenu : "http://172.16.174.75/workspace/PROJECTS/amdocs/amdocsweb/data/TrainingActivity/GetCategoryItemsMenu.php",
		
		categoryMenuBaseImgUrl : 'img/',
		
		GetClassItemsMenu : "http://172.16.174.75/workspace/PROJECTS/amdocs/amdocsweb/data/TrainingActivity/GetClassItemsMenu.php",
		
		categoryItemsbaseImgUrl : 'img/',
		
		getclasstrendingitemsmenu : 'http://172.16.174.75/workspace/PROJECTS/amdocs/amdocsweb/data/trainingactivity/getclasstrendingitemsmenu.php',
		
		getMoreActivityItemsMenu : 'http://172.16.174.75/workspace/PROJECTS/amdocs/amdocsweb/data/TrainingActivity/GetMoreActivityItemsMenu.php',
		
		bookItem : 'http://172.16.174.75/workspace/PROJECTS/amdocs/amdocsweb/data/UserTrainingActivity/BookUserTrainingActivity.php',
		
		bookClassItem : 'http://172.16.174.75/workspace/PROJECTS/amdocs/amdocsweb/data/UserTrainingActivity/BookUserTrainingActivity.php',
		
		bookVoucherItem : 'http://172.16.174.75/workspace/PROJECTS/amdocs/amdocsweb/data/UserTrainingActivity/BookUserTrainingActivity.php',
		
		getBookedItems : "http://172.16.174.75/workspace/PROJECTS/amdocs/amdocsweb/data/User/GetActivityBookedByUser.php",
		
		getBookedClassItems : "http://172.16.174.75/workspace/PROJECTS/amdocs/amdocsweb/data/User/GetActivityBookedByUser.php",
		
		getBookedVoucherItems : "http://172.16.174.75/workspace/PROJECTS/amdocs/amdocsweb/data/User/GetActivityBookedByUser.php",
		
		checkreferedBy : "http://172.16.174.75/workspace/PROJECTS/amdocs/amdocsweb/data/UserTrainingActivity/ReferredByFriend.php",
		
		referFriend : "http://172.16.174.75/workspace/PROJECTS/amdocs/amdocsweb/data/UserTrainingActivity/ReferFriend.php",
		
		changePassword : "http://172.16.174.75/workspace/PROJECTS/amdocs/amdocsweb/data/user/changepassword.php",
		
		cancelItem : "http://172.16.174.75/workspace/PROJECTS/amdocs/amdocsweb/data/UserTrainingActivity/CancelUserTrainingActivity.php",
		
		saveUserInfo : "http://172.16.174.75/workspace/PROJECTS/amdocs/amdocsweb/data/user/saveUserInfo.php",

		purchasePoints : "http://172.16.174.75/workspace/PROJECTS/amdocs/amdocsweb/data/user/purchasePoints.php",
		
		bookVoucher : "bookVoucher.html",
		
		bookingPage : "book.html",
		
		indexPage : "index.html",
		
		//*/
	}
};