$(document).ready(function() {

	$('#june').fullCalendar({
		theme: true,
		header: {
			left: "",
			center: 'title',
		},
		defaultDate: '2016-06-12',
		editable: true,
		eventLimit: true, // allow "more" link when too many events
		events: [
			{
				title: 'Bottle Art',
				start: '2016-06-01'
			},
			{
				title: 'Forge MMA',
				start: '2016-06-07'
			},

		]
	});
	
	$('#july').fullCalendar({
		theme: true,
		header: {
			left: "",
			center: 'title',
		},
		defaultDate: '2016-07-12',
		editable: true,
		eventLimit: true, // allow "more" link when too many events
		events: [
			{
				title: 'Bottle Art',
				start: '2016-07-01'
			},
			{
				title: 'Forge MMA',
				start: '2016-07-07'
			},

		]
	});
	
	$('#august').fullCalendar({
		theme: true,
		header: {
			left: "",
			center: 'title',
		},
		defaultDate: '2016-08-12',
		editable: true,
		eventLimit: true, // allow "more" link when too many events
		events: [
			{
				title: 'Bottle Art',
				start: '2016-08-01'
			},
			{
				title: 'Forge MMA',
				start: '2016-08-07'
			},

		]
	});
	
	$('#september').fullCalendar({
		theme: true,
		header: {
			left: "",
			center: 'title',
		},
		defaultDate: '2016-09-12',
		editable: true,
		eventLimit: true, // allow "more" link when too many events
		events: [
			{
				title: 'Bottle Art',
				start: '2016-09-01'
			},
			{
				title: 'Forge MMA',
				start: '2016-09-07'
			},

		]
	});
		
});