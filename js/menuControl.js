HTMLCollection.prototype.forEach = Array.prototype.forEach;

(function () {

    function Nav() {
        var navElement = document.getElementsByClassName("main-nav")[0],
			fixed = false,
			height = 1;

        window.addEventListener("scroll", function () {
            controlNav();
        });

        function controlNav() {
            if (window.pageYOffset > height && !fixed) {
                fixed = true;
                navElement.classList.add("fixed");
            } else if (window.pageYOffset <= height && fixed) {
                fixed = false;
                navElement.classList.remove("fixed");
            }
        }

        controlNav();
    }


    var helpline = {
        "init": function () {
            var cta = document.getElementById("helpline-Menu"),
                    container = document.getElementsByClassName("helpline-wrapper")[0],
                    closeBtn = document.getElementsByClassName("helpLineCloseBtn")[0],
                    width = container.offsetWidth,
                    speed = 500;
            console.log(2);
            cta.addEventListener("click", function () {
                Velocity(container, { right: ["0px", "-" + width + "px"] }, { duration: speed, easing: "spring" });
            });

            closeBtn.addEventListener("click", function () {
                Velocity(container, { right: ["-" + width + "px", "0px"] }, { duration: speed - 200 });
            });

        }
    }

    function FeedBack() {

        var _selfFeedback = this,
			element = document.getElementById("feedback"),
			feedbackClose = document.getElementById("feedbackClose"),
			feedbackTopic = document.getElementById("feedbackTopic"),
			feedbackString = document.getElementById("feedbackString"),
			feedBackForm = document.getElementById("feedBackForm"),
			topicString = document.getElementById("topicString"),
			charLeft = document.getElementById("charLeft"),
			thankyou = document.getElementById("thankyou"),
			thankyouClose = document.getElementById("thankyouClose"),
			selectedTopic;

        

        _selfFeedback.hide = function () {
            element.classList.remove("active");
        };

        _selfFeedback.show = function () {
            element.classList.add("active");
			charLeft.innerHTML = feedbackString.maxLength;
			feedbackString.value = "";
			feedbackString.disabled = true;
			
			feedbackTopic.children.forEach(function (elem) {
				elem.classList.remove("active");
				selectedTopic = undefined;
				
			});
			
        };

        feedbackClose.addEventListener("click", function () {
            _selfFeedback.hide();
        });

        feedbackTopic.children.forEach(function (elem) {
            elem.addEventListener("click", function () {
                if (selectedTopic != undefined)
                    selectedTopic.classList.remove("active");
                this.classList.add("active");
                selectedTopic = this;

                topicString.value = selectedTopic.getAttribute("data-name");

                if (feedbackString.disabled) {
                    feedbackString.disabled = false;
                    charLeft.innerHTML = feedbackString.maxLength;
                }
            });
        });

        feedBackForm.addEventListener("submit", function (e) {
            e.preventDefault();
            console.log(feedbackString.value.length);
            if (feedbackString.value.length == 0) {
                console.log("zero");
                return false;
            } else {
                console.log("Submit the form here");
                var result = Insertfeedback(topicString.value,feedbackString.value);
                element.classList.remove("active");
                thankyou.classList.add("active");
            }
            console.log(e);
        });

        feedbackString.addEventListener("input", function () {
            //console.log(this.value.length);
            charLeft.innerHTML = this.maxLength - this.value.length;
        });

        thankyouClose.addEventListener("click", function () {
            thankyou.classList.remove("active");
        });
    }


    window.addEventListener("DOMContentLoaded", function () {

        try {
            helpline.init();
        } catch (e) { }


        var menu = document.getElementsByClassName("menu")[0],
			mobileMenuTrigger = document.getElementsByClassName("mobile-menu-trigger")[0],
			menuCloseBtn = document.getElementsByClassName("menuCloseBtn")[0],
			feedBackCta = document.getElementById("feedbackCta");

        mobileMenuTrigger.addEventListener("click", function () {
            menu.style.right = "0px";
        });

        menuCloseBtn.addEventListener("click", function () {
            menu.style.right = "-320px";
        });

        var feedBack = new FeedBack(),
			nav = new Nav();

        feedBackCta.addEventListener("click", function () {
            feedBack.show();
        });

        if ($.fn.bxSlider != undefined) {

            console.log("not undefined");

            var slider = $(".slider").bxSlider({
                speed: 2000,
                auto: true,
                pager: false,
                controls: false,
                pause: 6000,
            });
			
			
        }
    });

})();

function Insertfeedback(feedbacktype, feedback) {
    var result;
    $.ajax({
        async: false,
        type: "GET",
        url: "/User/Insertfeedback?feedbacktype=" + feedbacktype + "&feedback=" + feedback,
        dataType: "json",
        success: function (data) {
            result = data;
            $("#feedbackString").val('');
        },
        cache: false,
        error: function (xhr, ajaxOptions, thrownError) {
            alert("Hi");
        }
    });
    return result;
}