//window.location.search.substring(1).split('&')[0].split("=")[1] //GEt first parameter value//
//This directive is for sliding input
app.directive("ngInputslider",function(){
	return {
		scope : {
			ngInputslider : "=",
			ngInputsliderOptions : "=",
			ngInputsliderChange : "=",
		},
		link : function(scope, $elem, $attr){

			var slideModel = scope.ngInputslider;

			var handleBar = $elem.find(".handle-bar");

			var slide = {
				width : $elem.width(),
				leftPos : handleBar.position().left,
				handleWidth : handleBar.width(),
				trackWidth : $elem.width() - handleBar.width(),
				valuePerc : 0
			}

			var evt = {
				startX : 0,
				mousedown : false
			}

			var modelValues = {}

			//console.log(slideModel)

			//console.log(handleBar);
			//console.log(slide);

			var digestTimeout = null;


			scope.$watch("ngInputsliderOptions",function(data){
				//console.log(data);
				
				modelValues.min = data[0];
				modelValues.max = data[1];
				modelValues.virMin = 0;
				modelValues.virMax = modelValues.max - modelValues.min;
				modelValues.value = slideModel.value = data[2];
				modelValues.virValue = modelValues.value - modelValues.min;
				modelValues.virValuePerc = Math.round(modelValues.virValue / modelValues.virMax * 100);
				slide.leftPos = Math.round(modelValues.virValuePerc / 100 * slide.trackWidth);
				handleBar.css({left : (slide.leftPos || 0)+"px"});
				//console.log(slide.leftPos);
			});


			handleBar.on("mousedown",function(e){
				//console.log(e);
				evt.startX = e.clientX;
				evt.mousedown = true;
			});

			$(document).on("mouseup",function(e){
				if(evt.mousedown == true){
					scope.ngInputsliderChange();
					evt.mousedown = false;
					scope.$apply();
				}

			});

			$(window).on("mousemove",function(e){
				if(evt.mousedown){

					//console.log(e);

					slide.leftPos += (e.clientX - evt.startX);
					evt.startX = e.clientX;
					slide.leftPos = (slide.leftPos > slide.trackWidth)?slide.trackWidth : (slide.leftPos < 0)? 0 : slide.leftPos;
					handleBar.css({left : slide.leftPos+"px"});
					slide.valuePerc = Math.round(slide.leftPos / slide.trackWidth * 100);
					
					slideModel.value = Math.round((slide.valuePerc / 100 * modelValues.virMax) + modelValues.min);
					
					
					// clearTimeout(digestTimeout);
					// digestTimeout = setTimeout(function(){
					// 	console.log(scope.ngInputsliderChange);
					// 	scope.ngInputsliderChange();
					// },50);
				}
			});

		}
	}
});


app.directive("ngEnter",function(){
	return {
		scope : {
			ngEnter : "="
		},
		link : function(scope, $elem, $attr){

			$elem.on("keypress",function(e){
				if(e.keyCode == 13){
					//console.log(e);
					if(scope.ngEnter)
						scope.ngEnter();
				}
			});
		}
	}
});


app.directive("ngCustombind",function(){
	return {
		scope : {
			ngCustombind : "="
		},
		link : function(scope, elem, attr){			
			scope.$watch('ngCustombind', function(newValue, oldValue) {
				elem.html(scope.ngCustombind);
				//scope.$apply();
			});
		}
	}
});

app.directive("ngNoPropa",function() {
	return {
		restrict : "A",
		link : function (scope, $elem, $attr) {
			$elem.on("click",function(e){
				e.stopPropagation();
			});
		}
	}
});