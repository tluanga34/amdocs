<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, PATCH, DELETE');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Headers: X-Requested-With,content-Type');
	
	//include('../sleep.php');
	
	
	$data = array();
	
	
	array_push($data, array(
		"Id" 		=> 1,
		"Type"		=> "class",
		"Name"		=> "Lantern Making",
		"BookDate" => "10/03/2017",
		"Duration" 	=> "5:00 PM-8:00 PM",
		"StartDate" => "12/03/2017",
		"Points" => "550",
		"ActivityImage" => "circuit.png",
		"TrainingActivityId" => "2069",
		"StartDateWithTime" => 48,
		"LocationName" => "Gurgaon",
		"CancelBeforeTime" => "6:00 PM",
		"CancelBeforeDate" => "12/02/2017",
		"disableCancel" => true
	));
	
	array_push($data, array(
		"Id" 		=> 2,
		"Type"		=> "class",
		"Name"		=> "Tai-chi",
		"BookDate" => "10/03/2017",
		"Duration" 	=> "3:00 PM-6:00 PM",
		"StartDate" => "29/03/2017",
		"Points" => "1000",
		"ActivityImage" => "Tai-Chi.png",
		"TrainingActivityId" => "2029",
		"StartDateWithTime" => 48,
		"LocationName" => "Gurgaon",
		"CancelBeforeTime" => "6:00 PM",
		"CancelBeforeDate" => "12/02/2017",
		"disableCancel" => false
	));
	
	array_push($data, array(
		"Id" 		=> 2,
		"Type"		=> "class",
		"Name"		=> "Aerobics",
		"BookDate" => "10/03/2017",
		"Duration" 	=> "3:00 PM-6:00 PM",
		"StartDate" => "29/03/2017",
		"Points" => "1000",
		"ActivityImage" => "Pilates.png",
		"TrainingActivityId" => "2029",
		"StartDateWithTime" => 48,
		"LocationName" => "Gurgaon",
		"CancelBeforeTime" => "6:00 PM",
		"CancelBeforeDate" => "12/02/2017",
		"disableCancel" => true
	));
	
	array_push($data, array(
		"Id" 		=> 2,
		"Type"		=> "class",
		"Name"		=> "Yoga",
		"BookDate" => "10/03/2017",
		"Duration" 	=> "3:00 PM-6:00 PM",
		"StartDate" => "29/03/2017",
		"Points" => "1000",
		"ActivityImage" => "Stretching.png",
		"TrainingActivityId" => "2029",
		"StartDateWithTime" => 48,
		"LocationName" => "Gurgaon",
		"CancelBeforeTime" => "6:00 PM",
		"CancelBeforeDate" => "12/02/2017",
		"disableCancel" => false
	));
	
	array_push($data, array(
		"Id" 		=> 2,
		"Type"		=> "class",
		"Name"		=> "Painting",
		"BookDate" => "10/03/2017",
		"Duration" 	=> "3:00 PM-6:00 PM",
		"StartDate" => "29/03/2017",
		"Points" => "1000",
		"ActivityImage" => "Pranayam.png",
		"TrainingActivityId" => "2029",
		"StartDateWithTime" => 48,
		"LocationName" => "Gurgaon",
		"CancelBeforeTime" => "6:00 PM",
		"CancelBeforeDate" => "12/02/2017",
		"disableCancel" => true
	));
	
	array_push($data, array(
		"Id" 		=> 3,
		"Type"		=> "voucher",
		"Name"		=> "Amazon Voucher",
		"BookDate" => "10/03/2017",
		//"Duration" 	=> "3:00 PM-6:00 PM",
		//"StartDate" => "29/03/2017",
		"Points" => "1000",
		"ActivityImage" => "voucher.jpg",
		"TrainingActivityId" => "321",
		"StartDateWithTime" => 48,
		//"LocationName" => "Gurgaon",
		"CancelBeforeTime" => "6:00 PM",
		"CancelBeforeDate" => "12/02/2017",
		"disableCancel" => false,
		"DeliverAddress" => "28 Biworldwide, Ulsoor Road, Ulsoor, Bangalore",
		"DeliverStatus" => "On the way"
	));
	
	array_push($data, array(
		"Id" 		=> 4,
		"Type"		=> "voucher",
		"Name"		=> "Flipkart Voucher",
		"BookDate" => "10/03/2017",
		//"Duration" 	=> "3:00 PM-6:00 PM",
		//"StartDate" => "29/03/2017",
		"Points" => "1500",
		"ActivityImage" => "voucher.jpg",
		"TrainingActivityId" => "122",
		"StartDateWithTime" => 48,
		//"LocationName" => "Gurgaon",
		"CancelBeforeTime" => "6:00 PM",
		"CancelBeforeDate" => "12/02/2017",
		"disableCancel" => true,
		"DeliverAddress" => "28 Biworldwide, Ulsoor Road, Ulsoor, Bangalore",
		"DeliverStatus" => "Delivered"
	));
	
	array_push($data, array(
		"Id" 		=> 5,
		"Type"		=> "voucher",
		"Name"		=> "BookMyShow Voucher",
		"BookDate" => "10/03/2017",
		//"Duration" 	=> "3:00 PM-6:00 PM",
		//"StartDate" => "29/03/2017",
		"Points" => "1500",
		"ActivityImage" => "voucher.jpg",
		"TrainingActivityId" => "122",
		//"StartDateWithTime" => 48,
		//"LocationName" => "Gurgaon",
		"CancelBeforeTime" => "6:00 PM",
		"CancelBeforeDate" => "12/02/2017",
		"disableCancel" => true,
		"DeliverAddress" => "28 Biworldwide, Ulsoor Road, Ulsoor, Bangalore",
		"DeliverStatus" => "Delivered"
	));

	
	echo json_encode($data);
	
?>