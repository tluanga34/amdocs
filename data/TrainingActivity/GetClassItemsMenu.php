<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, PATCH, DELETE');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Headers: X-Requested-With,content-type');
	
	include('../sleep.php');
	$data = array();
	
	if(isset($_GET['categoryId']) && $_GET['categoryId'] == 3){
		array_push($data, array(
			"Id" 		=> 2,
			"Name"		=> "Garba Freestyle_500",
			"Type"	=> "voucher",
			"Image1" => "voucher.jpg",
			"category" => "entertainment",
			"points"	=> 500
		));
		
		array_push($data, array(
			"Id" 		=> 3,
			"Name"		=> "Tollywood - Day 1_700",
			"Type"	=> "voucher",
			"Image1" => "voucher.jpg",
			"category" => "shopping",
			"points"	=> 700
		));
		
		array_push($data, array(
			"Id" 		=> 4,
			"Name"		=> "Tollywood - Day 2_200",
			"Type"	=> "voucher",
			"Image1" => "voucher.jpg",
			"category" => "entertainment",
			"points"	=> 200
		));
		
		array_push($data, array(
			"Id" 		=> 5,
			"Name"		=> "Tollywood - Day 3_1200",
			"Type"	=> "voucher",
			"Image1" => "voucher.jpg",
			"category" => "shopping",
			"points"	=> 1200
		));
		
		array_push($data, array(
			"Id" 		=> 5,
			"Name"		=> "Lavani_1400",
			"Type"	=> "voucher",
			"Image1" => "voucher.jpg",
			"category" => "entertainment",
			"points"	=> 1400
		));
		
		array_push($data, array(
			"Id" 		=> 5,
			"Name"		=> "Bhangra Fusion - Day 1_1600",
			"Type"	=> "voucher",
			"Image1" => "voucher.jpg",
			"category" => "shopping",
			"points"	=> 1600
		));
		
		array_push($data, array(
			"Id" 		=> 5,
			"Name"		=> "Bhangra Fusion - Day 2_1700",
			"Type"	=> "voucher",
			"Image1" => "voucher.jpg",
			"category" => "entertainment",
			"points"	=> 1700
		));
		
		array_push($data, array(
			"Id" 		=> 5,
			"Name"		=> "Caltural Fusion 3 Day Combo!_1900",
			"Type"	=> "voucher",
			"Image1" => "voucher.jpg",
			"category" => "shopping",
			"points"	=> 1900
		));
		
		array_push($data, array(
			"Id" 		=> 5,
			"Name"		=> "Freestyle Class- Day 2_2000",
			"Type"	=> "voucher",
			"Image1" => "voucher.jpg",
			"category" => "entertainment",
			"points"	=> 2000
		));
		
		array_push($data, array(
			"Id" 		=> 5,
			"Name"		=> "Amazon_2200",
			"Type"	=> "voucher",
			"Image1" => "voucher.jpg",
			"category" => "shopping",
			"points"	=> 2200
		));
		
		array_push($data, array(
			"Id" 		=> 5,
			"Name"		=> "Flipkart_2500",
			"Type"	=> "voucher",
			"Image1" => "voucher.jpg",
			"category" => "shopping",
			"points"	=> 2500
		));
		
		array_push($data, array(
			"Id" 		=> 5,
			"Name"		=> "LifeStyle_3200",
			"Type"	=> "voucher",
			"Image1" => "voucher.jpg",
			"category" => "shopping",
			"points"	=> 3200
		));
		
		array_push($data, array(
			"Id" 		=> 5,
			"Name"		=> "BookMyShow_3600",
			"Type"	=> "voucher",
			"Image1" => "voucher.jpg",
			"category" => "entertainment",
			"points"	=> 3600
		));
		
		array_push($data, array(
			"Id" 		=> 5,
			"Name"		=> "Fusion PUB_4000",
			"Type"	=> "voucher",
			"Image1" => "voucher.jpg",
			"category" => "entertainment",
			"points"	=> 4000
		));
		
		
		
	} else {
			
		array_push($data, array(
			"Id" 		=> 2,
			"Name"		=> "AEROBICS",
			"Type"	=> "class",
			"Image1" => "Aerobics.png"
		));
		
		array_push($data, array(
			"Id" 		=> 3,
			"Name"		=> "CIRCUIT",
			"Type"	=> "class",
			"Image1" => "circuit.png"
		));
		
		array_push($data, array(
			"Id" 		=> 4,
			"Name"		=> "KICKBOXING",
			"Type"	=> "class",
			"Image1" => "Kickboxing.png"
		));
		
		array_push($data, array(
			"Id" 		=> 5,
			"Name"		=> "MEDITATION",
			"Type"	=> "class",
			"Image1" => "Meditation.png"
		));
		
		array_push($data, array(
			"Id" 		=> 6,
			"Name"		=> "PRANAYAM",
			"Type"	=> "class",
			"Image1" => "Pranayam.png"
		));
		
		array_push($data, array(
			"Id" 		=> 7,
			"Name"		=> "Tai-Chi",
			"Type"	=> "class",
			"Image1" => "Tai-Chi.png"
		));
		
		array_push($data, array(
			"Id" 		=> 8,
			"Name"		=> "STRETCHING",
			"Type"	=> "class",
			"Image1" => "Stretching.png"
		));
		
		array_push($data, array(
			"Id" 		=> 9,
			"Name"		=> "SELF DEFENSE",
			"Type"	=> "class",
			"Image1" => "Self_Defense.png"
		));
		
		array_push($data, array(
			"Id" 		=> 9,
			"Name"		=> "YOGA",
			"Type"	=> "class",
			"Image1" => "Yoga.png"
		));
	}
	
	
	echo json_encode($data);
	
?>
