<?php
	header('Access-Control-Allow-Origin: *');
	header('Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, PATCH, DELETE');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Headers: X-Requested-With,content-type');
	
	
	$data = array();
	
			
	array_push($data, array(
		"Id" 		=> 2,
		"Name"		=> "AEROBICS",
		"Image1" => "Aerobics.png",
		"Type"	=> "class",
		"Image3" => "big_square.jpg"
	));
	
	array_push($data, array(
		"Id" 		=> 3,
		"Name"		=> "CIRCUIT",
		"Type"	=> "class",
		"Image1" => "circuit.png",
		"Image3" => "big_square.jpg"
	));
	
	array_push($data, array(
		"Id" 		=> 4,
		"Name"		=> "KICKBOXING",
		"Type"	=> "class",
		"Image1" => "Kickboxing.png",
		"Image3" => "big_square.jpg"
	));
	
	array_push($data, array(
		"Id" 		=> 5,
		"Name"		=> "MEDITATION",
		"Type"	=> "class",
		"Image1" => "Meditation.png",
		"Image3" => "big_square.jpg"
	));
	
	array_push($data, array(
		"Id" 		=> 6,
		"Name"		=> "PRANAYAM",
		"Type"	=> "class",
		"Image1" => "Pranayam.png",
		"Image3" => "big_square.jpg"
	));
		
	
	echo json_encode($data);
	
?>
